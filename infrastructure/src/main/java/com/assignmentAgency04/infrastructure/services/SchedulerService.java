package com.assignmentAgency04.infrastructure.services;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.concurrent.ScheduledFuture;

import com.assignmentAgency04.core.infrastructure.ISchedulerService;

import org.springframework.stereotype.Service;

@Service
public class SchedulerService implements ISchedulerService {

    Scheduler taskScheduler;

    public SchedulerService(Scheduler taskScheduler) {
        this.taskScheduler = taskScheduler;
    }

    @Override
    public void scheduleTask(Runnable task, Date date) {
        taskScheduler.threadPoolTaskScheduler().schedule(task,date);
    }

    @Override
    public void scheduleTask(Runnable task, Instant instant) {
        taskScheduler.threadPoolTaskScheduler().schedule(task,instant);
    }

    @Override
    public void scheduleTaskEveryUntil(Runnable task, long milliseconds, LocalDateTime until) {
        ScheduledFuture<?> future = taskScheduler.threadPoolTaskScheduler().scheduleAtFixedRate(task,milliseconds);
        // Instant instant = until.toInstant(ZoneOffset.UTC);
        Instant instant = ZonedDateTime.of(until, ZoneId.of("Europe/Zagreb")).toInstant();
        taskScheduler.threadPoolTaskScheduler().schedule(() -> {
            future.cancel(true);
        }, instant);
    }

    @Override
    public void scheduleTask(Runnable task, LocalDateTime localDateTime) {
        Instant instant = ZonedDateTime.of(localDateTime, ZoneId.of("Europe/Zagreb")).toInstant();
        taskScheduler.threadPoolTaskScheduler().schedule(task,instant);
    }
    
}
