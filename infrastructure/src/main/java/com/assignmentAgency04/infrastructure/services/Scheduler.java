package com.assignmentAgency04.infrastructure.services;

import java.util.IdentityHashMap;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import lombok.Getter;
import lombok.Setter;

@Configuration
@Getter
@Setter
public class Scheduler {

    private final Map<Object, ScheduledFuture<?>> scheduledTasks =
        new IdentityHashMap<>();

    @Bean
    TaskScheduler threadPoolTaskScheduler() {
        return new ThreadPoolTaskScheduler();
    }

    // @Override
    // public ScheduledFuture<?> scheduleAtFixedRate(Runnable task, long period) {
    //     ScheduledFuture<?> future = super.scheduleAtFixedRate(task, period);

    //     ScheduledMethodRunnable runnable = (ScheduledMethodRunnable) task;
    //     scheduledTasks.put(runnable.getTarget(), future);

    //     return future;
    // }

    // @Override
    // public ScheduledFuture<?> scheduleAtFixedRate(Runnable task, Date startTime, long period) {
    //     ScheduledFuture<?> future = super.scheduleAtFixedRate(task, startTime, period);

    //     ScheduledMethodRunnable runnable = (ScheduledMethodRunnable) task;
    //     scheduledTasks.put(runnable.getTarget(), future);

    //     return future;
    // }
}
