package com.assignmentAgency04.infrastructure.services.config;

import java.util.Properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import lombok.Getter;
import lombok.Setter;

@Configuration
@ConfigurationProperties(prefix = "mail")
@Getter
@Setter
public class EmailServiceConfig {
    // validation to properties
    private String host;
    private int port;
    private String username;
    private String password;
    private boolean auth;
    private boolean ttls;

    /*
    ● Server Name: ​email-smtp.eu-west-1.amazonaws.com 
    ● Port: ​25​, ​465​ or ​587 
    ● TLS: ​Yes 
    ● Authentication:  
        ○ SMTP Username: ​AKIA3QRJDSTT4P7LI2NJ 
        ○ SMTP Password: ​BDVKBQLtJH5DFJ3isJMP80afrFXxjyIOKlMNrdyHw7aD
    ● From: ​<team_name>@ag04.io
    */
    
    @Bean
    public JavaMailSender mailSender() {
        JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
        javaMailSender.setHost(host);
        javaMailSender.setPort(port);
        javaMailSender.setUsername(username);
        javaMailSender.setPassword(password);
        Properties props = javaMailSender.getJavaMailProperties();
        props.put("mail.smtp.auth", auth);
        props.put("mail.smtp.starttls.enable", ttls);
        return javaMailSender;
    }
}
