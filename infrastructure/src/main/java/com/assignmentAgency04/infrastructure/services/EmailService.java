package com.assignmentAgency04.infrastructure.services;

import com.assignmentAgency04.core.infrastructure.IEmailService;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailService implements IEmailService {
    
    private JavaMailSender javaMailSender;

    public EmailService(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Override
    public boolean sendEmail(String title, String body, String email) {
        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo(email);
        msg.setFrom("fabijan.zulj.assignment@ag04.com");

        msg.setSubject(title);
        msg.setText(body);
        try{
            // javaMailSender.send(msg);
            System.out.println("sendmail");
        } catch(Exception e) {
            return false;
        }
        return true;
    }
    
}
