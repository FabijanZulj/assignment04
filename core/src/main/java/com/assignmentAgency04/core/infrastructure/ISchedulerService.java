package com.assignmentAgency04.core.infrastructure;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Date;

public interface ISchedulerService {
    public void scheduleTask(Runnable task, Date date);
    public void scheduleTask(Runnable task, Instant instant);
    public void scheduleTask(Runnable task, LocalDateTime localDateTime);
    public void scheduleTaskEveryUntil(Runnable task, long milliseconds, LocalDateTime until);
}
