package com.assignmentAgency04.core.infrastructure;

public interface IEmailService {
    public boolean sendEmail(String title, String body, String email);
}
