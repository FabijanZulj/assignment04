package com.assignmentAgency04.core.exceptions;


public class DataIntegrityException extends Exception {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public DataIntegrityException(String errorMessage) {
        super(errorMessage);
    }
}