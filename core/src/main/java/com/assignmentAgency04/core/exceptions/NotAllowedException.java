package com.assignmentAgency04.core.exceptions;

public class NotAllowedException extends Exception{
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public NotAllowedException(String errorMessage) {
        super(errorMessage);
    }
}
