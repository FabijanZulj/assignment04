package com.assignmentAgency04.core.exceptions;

public class ValidationError extends Exception {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public ValidationError(String errorMessage) {
        super(errorMessage);
    }
}