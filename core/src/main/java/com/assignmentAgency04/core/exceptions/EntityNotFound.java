package com.assignmentAgency04.core.exceptions;


public class EntityNotFound extends Exception {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public EntityNotFound(String errorMessage) {
        super(errorMessage);
    }
}