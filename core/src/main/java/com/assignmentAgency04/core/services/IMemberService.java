package com.assignmentAgency04.core.services;

import com.assignmentAgency04.core.dto.MemberSkillsDTO;
import com.assignmentAgency04.core.dto.SkillsDTO;
import com.assignmentAgency04.core.entities.HeistMember;
import com.assignmentAgency04.core.exceptions.DataIntegrityException;
import com.assignmentAgency04.core.exceptions.EntityNotFound;
import com.assignmentAgency04.core.exceptions.ValidationError;


public interface IMemberService {
	public HeistMember getMemberById(String id) throws EntityNotFound;
	public HeistMember createMember(HeistMember heistMember) throws DataIntegrityException;
	public HeistMember updateSkills(SkillsDTO skills, String id) throws NumberFormatException, EntityNotFound, ValidationError, DataIntegrityException;
	public HeistMember deleteMembersSkill(String id, String skillName) throws NumberFormatException, EntityNotFound;
	public MemberSkillsDTO getMemberSkills(String memberId) throws NumberFormatException, EntityNotFound;
}
