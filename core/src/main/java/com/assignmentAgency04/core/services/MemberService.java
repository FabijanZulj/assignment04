package com.assignmentAgency04.core.services;

import org.springframework.stereotype.Service;

import java.util.List;

import com.assignmentAgency04.core.dto.MemberSkillsDTO;
import com.assignmentAgency04.core.dto.SkillsDTO;
import com.assignmentAgency04.core.entities.HeistMember;
import com.assignmentAgency04.core.entities.Skill;
import com.assignmentAgency04.core.exceptions.DataIntegrityException;
import com.assignmentAgency04.core.exceptions.EntityNotFound;
import com.assignmentAgency04.core.exceptions.ValidationError;
import com.assignmentAgency04.core.infrastructure.IEmailService;
import com.assignmentAgency04.core.repositories.IMemberRepository;
import com.assignmentAgency04.core.validators.MemberValidator;

@Service
public class MemberService implements IMemberService {

	IMemberRepository memberRepository;
	IEmailService emailService;
	MemberValidator memberValidator;

	public MemberService(IMemberRepository memberRepository, IEmailService emailService, MemberValidator memberValidator) {
		this.memberRepository = memberRepository;
		this.emailService = emailService;
		this.memberValidator = memberValidator;
	}

	@Override
	public HeistMember getMemberById(String id) throws EntityNotFound {
		return memberRepository.findById(Long.parseLong(id)).orElseThrow(() -> new EntityNotFound("Member not found"));
	}

	@Override
	public HeistMember createMember(HeistMember heistMember) throws DataIntegrityException {
		try {
			heistMember.getSkills().forEach((skill) -> {
				skill.setHeistMember(heistMember);
			});

			HeistMember savedHeistMember = memberRepository.save(heistMember);
			emailService.sendEmail("Added as member", "You have been added as member", heistMember.getEmail());
			return savedHeistMember;

		} catch (Exception e) {
			throw new DataIntegrityException("Data integrity exception");
		}
	}

	@Override
	public HeistMember updateSkills(SkillsDTO skills, String id)
			throws NumberFormatException, EntityNotFound, ValidationError, DataIntegrityException {
		if (memberValidator.validateSkills(skills)) {
			HeistMember heistMember = memberRepository.findById(Long.parseLong(id))
					.orElseThrow(() -> new EntityNotFound("Entity not found"));
			skills.getSkills().forEach(skill -> {
				skill.setHeistMember(heistMember);
			});
			heistMember.setSkills(skills.getSkills());
			heistMember.setMainSkill(skills.getMainSkill());
			try {
				return memberRepository.save(heistMember);
			} catch (Exception e) {
				throw new DataIntegrityException("Data integrity exception" + e.getLocalizedMessage());
			}
		} else {
			throw new ValidationError("Validation error");
		}
	}

	@Override
	public HeistMember deleteMembersSkill(String id, String skillName) throws NumberFormatException, EntityNotFound {
		HeistMember heistMember = memberRepository.findById(Long.parseLong(id))
				.orElseThrow(() -> new EntityNotFound("Entity not found"));
		List<Skill> listOfSkills = heistMember.getSkills();
		if (!listOfSkills.removeIf(skill -> skill.getName().equals(skillName))) {
			throw new EntityNotFound("Skill not found");
		} else {
			return memberRepository.save(heistMember);
		}
	}

	@Override
	public MemberSkillsDTO getMemberSkills(String memberId) throws NumberFormatException, EntityNotFound {
		HeistMember member = memberRepository.findById(Long.parseLong(memberId))
				.orElseThrow(() -> new EntityNotFound("Entity not found"));
		return new MemberSkillsDTO(member.getSkills(), member.getMainSkill());
	}

}
