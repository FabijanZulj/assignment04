package com.assignmentAgency04.core.services;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import com.assignmentAgency04.core.dto.EligibleMembersDTO;
import com.assignmentAgency04.core.dto.HeistSkillsDTO;
import com.assignmentAgency04.core.entities.Heist;
import com.assignmentAgency04.core.entities.HeistMember;
import com.assignmentAgency04.core.entities.HeistStatus;
import com.assignmentAgency04.core.entities.MemberStatus;
import com.assignmentAgency04.core.entities.Skill;
import com.assignmentAgency04.core.entities.SkillsHeist;
import com.assignmentAgency04.core.exceptions.BadRequestException;
import com.assignmentAgency04.core.exceptions.DataIntegrityException;
import com.assignmentAgency04.core.exceptions.EntityNotFound;
import com.assignmentAgency04.core.exceptions.NotAllowedException;
import com.assignmentAgency04.core.exceptions.UpdateNotAllowed;
import com.assignmentAgency04.core.exceptions.ValidationError;
import com.assignmentAgency04.core.infrastructure.IEmailService;
import com.assignmentAgency04.core.infrastructure.ISchedulerService;
import com.assignmentAgency04.core.repositories.IHeistRepository;
import com.assignmentAgency04.core.repositories.IMemberRepository;
import com.assignmentAgency04.core.validators.HeistValidator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Service
public class HeistService implements IHeistService {
    private static final Logger logger = LoggerFactory.getLogger(HeistService.class);

    private IHeistRepository heistRepository;
    private IMemberRepository memberRepository;
    private IEmailService emailService;
    private ISchedulerService schedulerService;
    @Value("${levelUpTime}")
    private int levelUpTime;
    private HeistValidator heistValidator;

    public HeistService(IHeistRepository heistRepository, IMemberRepository memberRepository,
            IEmailService emailService, ISchedulerService schedulerService, HeistValidator heistValidator) {
        this.heistRepository = heistRepository;
        this.memberRepository = memberRepository;
        this.emailService = emailService;
        this.schedulerService = schedulerService;
        this.heistValidator = heistValidator;
    }

    @Override
    public Heist createHeist(Heist heist) throws ValidationError, DataIntegrityException {

        if (heistValidator.validateHeist(heist)) {
            try {
                heist.getSkills().forEach(heistSkill -> {
                    heistSkill.setHeist(heist);
                });
                Heist savedHeist = heistRepository.save(heist);

                // Automatic heist start
                schedulerService.scheduleTask(() -> {
                    try {
                        logger.info("Automaticaly starting heist, Heist ID: " + savedHeist.getId());
                        startHeist(savedHeist.getId().toString());
                    } catch (Exception e) {
                        logger.error("Error while starting heist, Heist ID: " + savedHeist.getId() + " /Message: "
                                + e.getLocalizedMessage());
                    }
                }, savedHeist.getStartTime());

                // Automatic heist ending
                schedulerService.scheduleTask(() -> {
                    try {
                        logger.info("Automaticaly ending heist, Heist ID: " + savedHeist.getId());
                        endHeist(savedHeist.getId().toString());
                    } catch (Exception e) {
                        logger.error("Error while ending heist, Heist ID: " + savedHeist.getId() + " /Message: "
                                + e.getLocalizedMessage());
                    }
                }, savedHeist.getEndTime());

                return savedHeist;
            } catch (Exception e) {
                throw new DataIntegrityException("Data integrity exception");
            }
        } else {
            throw new ValidationError("Validation error");
        }
    }

    @Override
    public Heist updateSkills(HeistSkillsDTO heistSkills, String id)
            throws NumberFormatException, EntityNotFound, UpdateNotAllowed, DataIntegrityException, ValidationError {
        if (heistValidator.validateHeistSkills(heistSkills.getSkills())) {
            Heist heist = heistRepository.findById(Long.parseLong(id))
                    .orElseThrow(() -> new EntityNotFound("Entity not found"));
            if (heist.getStartTime().isBefore(LocalDateTime.now()) || heist.getStatus() == HeistStatus.IN_PROGRESS) {
                throw new UpdateNotAllowed("Heist is already in progress");
            }
            heistSkills.getSkills().forEach(skill -> {
                skill.setHeist(heist);
            });
            heist.updateSkills(heistSkills.getSkills());
            try {
                return heistRepository.save(heist);
            } catch (Exception e) {
                throw new DataIntegrityException("Data integrity exception");
            }
        } else {
            throw new ValidationError("Recieved heist skills not valid");
        }
    }

    @Override
    public EligibleMembersDTO getEligibleMembers(String heistId) throws NumberFormatException, EntityNotFound,
            NotAllowedException {
        Heist heist = heistRepository.findById(Long.parseLong(heistId))
            .orElseThrow(() -> new EntityNotFound("Entity not found"));
        if(heist.getStatus() == HeistStatus.READY) {
            throw new NotAllowedException("Heist members have already been confirmed");
        }
        List<SkillsHeist> heistSkills = heistRepository.getHeistSkills(Long.parseLong(heistId));
        List<HeistMember> eligibleHeistMembers = heistRepository.getEligibleHeistMembers(Long.parseLong(heistId));
        return new EligibleMembersDTO(heistSkills, eligibleHeistMembers);
    }

    @Override
    public Heist confirmMembers(List<String> members, String heistId)
            throws NumberFormatException, EntityNotFound , NotAllowedException, BadRequestException {
        List<HeistMember> eligibleHeistMembers = heistRepository.getEligibleHeistMembers(Long.parseLong(heistId));
        Heist heist = heistRepository.findById(Long.parseLong(heistId))
                .orElseThrow(() -> new EntityNotFound("Entity not found"));
        if (heist.getStatus() != HeistStatus.PLANNING) {
            throw new NotAllowedException("Heist not in PLANNING");
        }
        List<String> eligibleMembersNames = eligibleHeistMembers.stream().map(m -> m.getName())
                .collect(Collectors.toList());
        if (eligibleMembersNames.containsAll(members)) {
            List<String> membersAlreadyOnAHeist = heistRepository.getAllMembersInAHeist(Long.parseLong(heistId),
                    heist.getStartTime());
            membersAlreadyOnAHeist.retainAll(members);
            if (membersAlreadyOnAHeist.size() == 0) {
                heist.setConfirmedMembers(members);
                heist.setStatus(HeistStatus.READY);
                List<HeistMember> confirmedMembersToSendMailTo = eligibleHeistMembers.stream()
                        .filter((HeistMember member) -> {
                            return members.contains(member.getName());
                        }).collect(Collectors.toList());
                for (HeistMember memberToSendMailTo : confirmedMembersToSendMailTo) {
                    emailService.sendEmail("Confirmed for a heist", "You have been confirmed as a member of a heist",
                            memberToSendMailTo.getEmail());
                }
                heistRepository.save(heist);
            } else {
                throw new BadRequestException("Some members are already on a heist");
            }
        } else {
            throw new BadRequestException("Users not eligible for the heist");
        }
        return heist;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Heist startHeist(String heistId) throws NumberFormatException, EntityNotFound,
            NotAllowedException {
        Heist heist = heistRepository.findById(Long.parseLong(heistId))
                .orElseThrow(() -> new EntityNotFound("Entity not found"));
        if (heist.getStatus() != HeistStatus.READY) {
            throw new NotAllowedException("Heist status is not ready, its" + heist.getStatus());
        } else {
            heist.setStatus(HeistStatus.IN_PROGRESS);
            List<HeistMember> heistMembersToSendMailTo = memberRepository
                    .getConfirmedHeistMembers(heist.getConfirmedMembers());
            for (HeistMember memberToSendMailTo : heistMembersToSendMailTo) {
                emailService.sendEmail("Heist started", "Heist you are on has started", memberToSendMailTo.getEmail());
            }

            schedulerService.scheduleTaskEveryUntil(() -> {
                try{
                    increaseLevelOfMembers(heist.getConfirmedMembers(), heist);

                } catch (Exception e) {
                    logger.error("Error while increasing heist member skill level"+ e.getLocalizedMessage());
                }
            }, levelUpTime*1000, heist.getEndTime());
            return heistRepository.save(heist);
        }
    }

    @Transactional()
    public void increaseLevelOfMembers(List<String> listOfConfirmedMembers, Heist heist)
            throws NumberFormatException, EntityNotFound {
        logger.info("Increasing heist member level");
        List<HeistMember> heistMembers = memberRepository.getConfirmedHeistMembers(listOfConfirmedMembers);
        List<SkillsHeist> heistsSkills = heistRepository.getHeistSkills(heist.getId());
        List<String> skillNamesOnAHeist = heistsSkills.stream().map((skill) -> skill.getName()).collect(Collectors.toList());


        for(HeistMember member : heistMembers) {
            HeistMember hm = memberRepository.getMemberWithSkills(member.getId());
            List<Skill> memberSkills = hm.getSkills();
            for(Skill sk : memberSkills) {
                if(skillNamesOnAHeist.contains(sk.getName())) {
                    if(sk.getLevel().length() < 10) {
                        sk.setLevel(sk.getLevel()+ "*");
                    }
                }
            }
            memberRepository.save(hm);
        }
    }

    @Override
    @Transactional
    public Heist endHeist(String heistId) throws NumberFormatException, EntityNotFound, ValidationError {
        Heist heist = heistRepository.findById(Long.parseLong(heistId))
            .orElseThrow(() -> new EntityNotFound("Entity not found"));
        if (heist.getStatus() != HeistStatus.IN_PROGRESS) {
            throw new ValidationError("Not a validatiopn, heist is not in progress");
        } else {
            heist.setStatus(HeistStatus.FINISHED);
            List<HeistMember> heistMembersToSendMailTo = memberRepository.getConfirmedHeistMembers(heist.getConfirmedMembers());
            for(HeistMember memberToSendMailTo : heistMembersToSendMailTo) {
                emailService.sendEmail("Heist finished", "Heist you are on has finished", memberToSendMailTo.getEmail());
            }
            return heistRepository.save(heist);
        }
    }

    @Override
    public Heist getHeistById(String heistId) throws NumberFormatException, EntityNotFound {
        return heistRepository.findById(Long.parseLong(heistId))
                .orElseThrow(() -> new EntityNotFound("Entity not found"));
    }

    @Override
    public List<HeistMember> getConfirmedHeistMembers(String heistId)
            throws NumberFormatException, EntityNotFound, NotAllowedException {
        Heist heist = heistRepository.findById(Long.parseLong(heistId))
                .orElseThrow(() -> new EntityNotFound("Entity not found"));
        if (heist.getStatus() == HeistStatus.PLANNING) {
            throw new NotAllowedException("Heist in planning, cant get members");
        }
        return memberRepository.getConfirmedHeistMembers(heist.getConfirmedMembers());
    }


    @Override
    public List<SkillsHeist> getHeistSkills(String heistId) throws NumberFormatException, EntityNotFound {
        heistRepository.findById(Long.parseLong(heistId))
                .orElseThrow(() -> new EntityNotFound("Entity not found"));
        
        return heistRepository.getHeistSkills(Long.parseLong(heistId));
    }

    @Override
    public Map<String, HeistStatus> getHeistStatus(String heistId) throws NumberFormatException, EntityNotFound {
        Heist heist = heistRepository.findById(Long.parseLong(heistId))
                .orElseThrow(() -> new EntityNotFound("Entity not found"));
        return new HashMap<String, HeistStatus>() {
            private static final long serialVersionUID = 1L;

            {
                put("status", heist.getStatus());
            }
        };
    }

    @Override
    public Map<String, String> getHeistOutcome(String heistId)
            throws NumberFormatException, EntityNotFound, UpdateNotAllowed {
        Heist heist = heistRepository.findById(Long.parseLong(heistId))
                .orElseThrow(() -> new EntityNotFound("Entity not found"));
        if (heist.getStatus() != HeistStatus.FINISHED) {
            throw new UpdateNotAllowed("errorMessage");
        }
        int requiredMembers = heist.getSkills().stream().mapToInt(SkillsHeist::getMembers).sum();
        int membersPresent = heist.getConfirmedMembers().size();
        MemberStatus[] memberStatus = MemberStatus.values();

        if (membersPresent < 0.5 * requiredMembers) {

            // ALL MEMBERS EXPIRED OR INCARCERATED
            int[] numbers = new Random().ints(1, 2).limit(membersPresent).toArray();
            List<HeistMember> heistMembersToUpdate = memberRepository
                    .getConfirmedHeistMembers(heist.getConfirmedMembers());
            int index = 0;
            for (HeistMember h : heistMembersToUpdate) {
                h.setStatus(memberStatus[numbers[index]]);
                index++;
            }

            memberRepository.saveAll(heistMembersToUpdate);

            return new HashMap<String, String>() {
                private static final long serialVersionUID = 1L;
                {
                    put("status", "FAILED");
                }
            };
            // OUTCOME = FAILED

        } else if (membersPresent >= 0.5 * requiredMembers && membersPresent < 0.75 * requiredMembers) {
            if (new Random().nextInt(2) == 0) {
                int[] statusIndex = new Random().ints(1, 2).limit(membersPresent).toArray();
                List<HeistMember> heistMembersToUpdate = memberRepository
                        .getConfirmedHeistMembers(heist.getConfirmedMembers());
                int[] indexesOfMembersWithRepercussions = new Random().ints(0, membersPresent).distinct()
                        .limit(membersPresent * (2 / 3)).toArray();
                int loopIndex = 0;
                for (int i : indexesOfMembersWithRepercussions) {
                    heistMembersToUpdate.get(i).setStatus(memberStatus[statusIndex[loopIndex]]);
                    loopIndex++;
                }
                // FAILED 2/3 member EXPIRED OR INCARCERATED
                return new HashMap<String, String>() {
                    private static final long serialVersionUID = 1L;
                    {
                        put("status", "FAILED");
                    }
                };
            } else {
                // SUCCEEDED AND 1/3 MEMBERS EXPIRED OR INCARCERATED
                int[] statusIndex = new Random().ints(1, 2).limit(membersPresent).toArray();
                List<HeistMember> heistMembersToUpdate = memberRepository
                        .getConfirmedHeistMembers(heist.getConfirmedMembers());
                int[] indexesOfMembersWithRepercussions = new Random().ints(0, membersPresent).distinct()
                        .limit(membersPresent * (1 / 3)).toArray();
                int loopIndex = 0;
                for (int i : indexesOfMembersWithRepercussions) {
                    heistMembersToUpdate.get(i).setStatus(memberStatus[statusIndex[loopIndex]]);
                    loopIndex++;
                }
                return new HashMap<String, String>() {
                    private static final long serialVersionUID = 1L;
                    {
                        put("status", "SUCEEDED");
                    }
                };
            }
            // EITHER 2/3 members EXPIRED OR INCARCERATED AND FAILED
            // OR 1/3 members EXPIRED OR INCARCERATED AND SUCCEEDED
        } else if (membersPresent >= 0.75 * requiredMembers && membersPresent < 1 * requiredMembers) {
            // 1/3 MEMBERS INCARCERATED
            // SUCEEDED
            List<HeistMember> heistMembersToUpdate = memberRepository
                    .getConfirmedHeistMembers(heist.getConfirmedMembers());
            int[] indexesOfMembersWithRepercussions = new Random().ints(0, membersPresent).distinct()
                    .limit(membersPresent * (1 / 3)).toArray();

            for (int i : indexesOfMembersWithRepercussions) {
                heistMembersToUpdate.get(i).setStatus(MemberStatus.INCARCERATED);
            }
            return new HashMap<String, String>() {
                private static final long serialVersionUID = 1L;
                {
                    put("status", "SUCEEDED");
                }
            };
        } else {
            // SUCEEDED
            return new HashMap<String, String>() {
                private static final long serialVersionUID = 1L;
                {
                    put("status", "SUCEEDED");
                }
            };
        }
    }

}
