package com.assignmentAgency04.core.services;

import java.util.List;
import java.util.Map;

import com.assignmentAgency04.core.dto.EligibleMembersDTO;
import com.assignmentAgency04.core.dto.HeistSkillsDTO;
import com.assignmentAgency04.core.entities.Heist;
import com.assignmentAgency04.core.entities.HeistMember;
import com.assignmentAgency04.core.entities.HeistStatus;
import com.assignmentAgency04.core.entities.SkillsHeist;
import com.assignmentAgency04.core.exceptions.BadRequestException;
import com.assignmentAgency04.core.exceptions.DataIntegrityException;
import com.assignmentAgency04.core.exceptions.EntityNotFound;
import com.assignmentAgency04.core.exceptions.NotAllowedException;
import com.assignmentAgency04.core.exceptions.UpdateNotAllowed;
import com.assignmentAgency04.core.exceptions.ValidationError;

public interface IHeistService {
    public Heist getHeistById(String heistId) throws NumberFormatException, EntityNotFound;
    public Heist createHeist(Heist heist) throws ValidationError, DataIntegrityException;
    public Heist updateSkills(HeistSkillsDTO heistSkills, String id) throws NumberFormatException, EntityNotFound, UpdateNotAllowed, DataIntegrityException, ValidationError;
    public EligibleMembersDTO getEligibleMembers(String heistId) throws NumberFormatException, EntityNotFound, NotAllowedException;
    public Heist confirmMembers(List<String> members, String heistId) throws NumberFormatException, EntityNotFound, UpdateNotAllowed, NotAllowedException, BadRequestException;
    public Heist startHeist(String heistId) throws NumberFormatException, EntityNotFound, NotAllowedException;
    public List<HeistMember> getConfirmedHeistMembers(String heistId) throws NumberFormatException, EntityNotFound, NotAllowedException;
    public List<SkillsHeist> getHeistSkills(String heistId) throws NumberFormatException, EntityNotFound;
    public Map<String,HeistStatus> getHeistStatus(String heistId) throws NumberFormatException, EntityNotFound;
    public Map<String,String> getHeistOutcome(String heistId) throws NumberFormatException, EntityNotFound, UpdateNotAllowed;
    public Heist endHeist(String heistId) throws NumberFormatException, EntityNotFound, ValidationError;
}
