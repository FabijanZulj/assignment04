package com.assignmentAgency04.core.dto;

import java.util.List;

import com.assignmentAgency04.core.entities.SkillsHeist;

import lombok.Data;

@Data
public class HeistSkillsDTO {
    List<SkillsHeist> skills; 
}
