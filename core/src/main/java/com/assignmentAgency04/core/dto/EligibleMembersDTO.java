package com.assignmentAgency04.core.dto;

import java.util.List;

import com.assignmentAgency04.core.entities.HeistMember;
import com.assignmentAgency04.core.entities.SkillsHeist;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EligibleMembersDTO {
    List<SkillsHeist> skills;
    List<HeistMember> members;
}
