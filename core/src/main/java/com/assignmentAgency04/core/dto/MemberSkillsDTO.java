package com.assignmentAgency04.core.dto;

import java.util.List;

import com.assignmentAgency04.core.entities.Skill;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class MemberSkillsDTO {
    private List<Skill> memberSkills;
    private String mainSkill;
}
