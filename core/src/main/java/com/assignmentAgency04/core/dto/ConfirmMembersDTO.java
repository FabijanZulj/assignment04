package com.assignmentAgency04.core.dto;

import java.util.List;

import lombok.Data;

@Data
public class ConfirmMembersDTO {
    private List<String> members; 
}
