package com.assignmentAgency04.core.dto;

import java.util.List;

import com.assignmentAgency04.core.entities.Skill;

import lombok.Data;

@Data
public class SkillsDTO {
    private List<Skill> skills;
    private String mainSkill;
}
