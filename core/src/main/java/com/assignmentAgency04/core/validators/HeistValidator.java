package com.assignmentAgency04.core.validators;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.assignmentAgency04.core.entities.Heist;
import com.assignmentAgency04.core.entities.SkillsHeist;

import org.springframework.stereotype.Component;

import lombok.NoArgsConstructor;

@NoArgsConstructor
@Component
public class HeistValidator {


    public boolean validateHeistSkills(List<SkillsHeist> listOfSkills) {
        Set<SkillsHeist> setOfSkills = new HashSet<SkillsHeist>(listOfSkills);
        if (setOfSkills.size() < listOfSkills.size()) {
            return false;
        } else {
            return true;
        }
    }


    public boolean validateHeist(Heist heist) {
        if (!validateHeistSkills(heist.getSkills())) {
            return false;
        }
        if (heist.getStartTime().isAfter(heist.getEndTime())) {
            return false;
        }
        if (heist.getEndTime().isBefore(LocalDateTime.now())) {
            return false;
        }
        return true;
    }
}
