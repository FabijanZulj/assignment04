package com.assignmentAgency04.core.validators;

import com.assignmentAgency04.core.dto.SkillsDTO;
import com.assignmentAgency04.core.entities.Skill;

import org.springframework.stereotype.Component;

import lombok.NoArgsConstructor;

@NoArgsConstructor
@Component
public class MemberValidator {
    public boolean validateSkills(SkillsDTO skills) {
		boolean isValid = false;
		for (Skill skill : skills.getSkills()) {
			if (skill.getName().equals(skills.getMainSkill())) {
				isValid = true;
			}
		}
		return isValid;
	}
}
