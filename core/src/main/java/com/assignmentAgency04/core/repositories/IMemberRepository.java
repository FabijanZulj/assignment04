package com.assignmentAgency04.core.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

import com.assignmentAgency04.core.entities.HeistMember;
import com.assignmentAgency04.core.entities.Skill;


public interface IMemberRepository extends CrudRepository<HeistMember, Long> {

    @Query("SELECT sk FROM HeistMember hm JOIN hm.skills sk WHERE hm.id = ?1")
    public List<Skill> getHeistMemberSkills(Long memberId);

    @Query("SELECT m FROM HeistMember m LEFT JOIN FETCH m.skills ms WHERE m.id =?1")
    public HeistMember getMemberWithSkills(long memberId);

    @Query("SELECT m FROM HeistMember m WHERE m.name IN ?1")
    List<HeistMember> getConfirmedHeistMembers(List<String> confirmedHeistMembers);

}
