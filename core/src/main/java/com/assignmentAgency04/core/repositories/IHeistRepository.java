package com.assignmentAgency04.core.repositories;

import java.time.LocalDateTime;
import java.util.List;

import com.assignmentAgency04.core.entities.Heist;
import com.assignmentAgency04.core.entities.HeistMember;
import com.assignmentAgency04.core.entities.SkillsHeist;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface IHeistRepository extends CrudRepository<Heist, Long> {

    @Query("SELECT hs FROM Heist h JOIN h.skills hs WHERE h.id = ?1")
    List<SkillsHeist> getHeistSkills(Long heistId);

    @Query("SELECT DISTINCT m FROM Skill s JOIN s.heistMember m LEFT JOIN SkillsHeist sh ON(sh.heist.id = 1) WHERE s.name = sh.name AND LENGTH(s.level) >= LENGTH(sh.level) AND m.status = 0 OR m.status = 3")
    List<HeistMember> getEligibleHeistMembers(Long heistId);

    @Query("SELECT cm FROM Heist h JOIN h.confirmedMembers cm WHERE h.id = ?1 AND h.startTime <= ?2 AND h.endTime >= ?2")
    List<String> getAllMembersInAHeist(Long heistId, LocalDateTime startTime);

    

}
