package com.assignmentAgency04.core.entities;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Skill {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	// @Column(unique=true)
	private String name;
	@Size(max=10, message="Maximum number of stars is 10")
	private String level = "*";
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonBackReference
    @ToString.Exclude
	private HeistMember heistMember;
	

	@Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (obj.getClass() != this.getClass()) {
            return false;
        }
        final Skill other = (Skill) obj;
        if(name.equals(other.getName()) && level.equals(other.getLevel())){
            return true;
        }
        return false;
    }
}
