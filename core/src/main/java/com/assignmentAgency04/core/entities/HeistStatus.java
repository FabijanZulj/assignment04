package com.assignmentAgency04.core.entities;


public enum HeistStatus {
	PLANNING,
	READY,
	IN_PROGRESS,
	FINISHED
}