package com.assignmentAgency04.core.entities;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Heist {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    @Column(unique=true)
    @NotNull
    private String name;
    @NotNull
    private String location;
    @NotNull
    private LocalDateTime startTime;
    @NotNull 
    private LocalDateTime endTime;
    private HeistStatus status = HeistStatus.PLANNING;

    @JsonManagedReference
    @OneToMany(cascade={CascadeType.ALL}, orphanRemoval = true, mappedBy = "heist")
    @NotNull
    private List<SkillsHeist> skills;

    @ElementCollection(fetch = FetchType.EAGER)
    private List<String> confirmedMembers;

    public void updateSkills(List<SkillsHeist> updatedSkills) {
        skills.clear();
        skills.addAll(updatedSkills);
    }
}
