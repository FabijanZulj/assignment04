package com.assignmentAgency04.core.entities;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class SkillsHeist {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    @NotNull
    private String name;
    @NotNull
    private String level;
    @NotNull
    private int members;
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonBackReference
    private Heist heist;


    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (obj.getClass() != this.getClass()) {
            return false;
        }
        final SkillsHeist other = (SkillsHeist) obj;
        if(name.equals(other.getName()) && level.equals(other.getLevel())){
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return String.format("%s%s", this.name, this.level).hashCode();
    }
}
