package com.assignmentAgency04.core.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class HeistMember {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	@NotNull
	private String name;
	@NotNull
	private Sex sex;
	@Column(unique=true)
	private String email;
	@NotNull
	@JsonManagedReference
	@OneToMany(cascade=CascadeType.ALL, mappedBy = "heistMember", orphanRemoval = true)
	private List<Skill> skills = null;

	private String mainSkill;
	@NotNull
	private MemberStatus status;


	public void setSkills(List<Skill> newSkills) {
		if (this.skills == null) {
			this.skills = new ArrayList<Skill>();
			this.skills.addAll(newSkills);
		  } else {
			this.skills.clear();
		   this.skills.addAll(newSkills);
		  }
		// skills.clear();
		// skills.addAll(newSkills);
	}
	
}

