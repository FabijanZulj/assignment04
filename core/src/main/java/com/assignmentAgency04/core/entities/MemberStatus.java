package com.assignmentAgency04.core.entities;


public enum MemberStatus {
    AVAILABLE,
    EXPIRED,
    INCARCERATED,
    RETIRED
}
