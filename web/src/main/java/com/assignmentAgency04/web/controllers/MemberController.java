package com.assignmentAgency04.web.controllers;

import javax.validation.Valid;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.assignmentAgency04.core.dto.MemberSkillsDTO;
import com.assignmentAgency04.core.dto.SkillsDTO;
import com.assignmentAgency04.core.entities.HeistMember;
import com.assignmentAgency04.core.exceptions.DataIntegrityException;
import com.assignmentAgency04.core.exceptions.EntityNotFound;
import com.assignmentAgency04.core.exceptions.ValidationError;
import com.assignmentAgency04.core.services.IMemberService;

@RestController
public class MemberController {
	IMemberService memberService;

	public MemberController(IMemberService _memberService) {
		memberService = _memberService;
	}

	@GetMapping("/member/{id}")
	public ResponseEntity<HeistMember> getMember(@PathVariable String id) throws EntityNotFound {
		HeistMember heistMember = memberService.getMemberById(id);
		return new ResponseEntity<HeistMember>(heistMember,HttpStatus.OK);
	}

	@PostMapping("/member")
	public ResponseEntity<Void> saveMember(@RequestBody @Valid HeistMember heistMember) throws DataIntegrityException {
		Long id = memberService.createMember(heistMember).getId();
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.set("Location", String.format("/member/%s", id));
		return new ResponseEntity<>(responseHeaders, HttpStatus.CREATED);
	}

	@PutMapping("/member/{id}/skill")
	public ResponseEntity<Void> updateSkills(@RequestBody SkillsDTO skills, @PathVariable String id)
			throws NumberFormatException, EntityNotFound, ValidationError, DataIntegrityException {
		memberService.updateSkills(skills, id);
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.set("Content-Location", String.format("/member/%s/skills", id));
		return new ResponseEntity<>(responseHeaders,HttpStatus.NO_CONTENT);
	}


	@GetMapping("/member/{id}/skills")
	public MemberSkillsDTO getSkills(@PathVariable String id) throws NumberFormatException, EntityNotFound {
		// GEt skills and main skill for member
		return memberService.getMemberSkills(id);
	}

	
	@DeleteMapping("/member/{id}/skills/{skillName}")
	public ResponseEntity<Void> deleteMembersSkill(@PathVariable String id, @PathVariable String skillName)
			throws NumberFormatException, EntityNotFound {
		memberService.deleteMembersSkill(id, skillName);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
}
