package com.assignmentAgency04.web.controllers;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.assignmentAgency04.core.dto.ConfirmMembersDTO;
import com.assignmentAgency04.core.dto.EligibleMembersDTO;
import com.assignmentAgency04.core.dto.HeistSkillsDTO;
import com.assignmentAgency04.core.entities.Heist;
import com.assignmentAgency04.core.entities.HeistMember;
import com.assignmentAgency04.core.entities.HeistStatus;
import com.assignmentAgency04.core.entities.SkillsHeist;
import com.assignmentAgency04.core.exceptions.BadRequestException;
import com.assignmentAgency04.core.exceptions.DataIntegrityException;
import com.assignmentAgency04.core.exceptions.EntityNotFound;
import com.assignmentAgency04.core.exceptions.NotAllowedException;
import com.assignmentAgency04.core.exceptions.UpdateNotAllowed;
import com.assignmentAgency04.core.exceptions.ValidationError;
import com.assignmentAgency04.core.services.IHeistService;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HeistController {
    IHeistService heistService;

    public HeistController(IHeistService heistService) {
        this.heistService = heistService;
    }

    @GetMapping("/heist/{id}")
    public Heist getHeist(@PathVariable String id) throws NumberFormatException, EntityNotFound {
        return heistService.getHeistById(id);
    }

    @GetMapping("/heist/{id}/members")
    public List<HeistMember> getHeistMembers(@PathVariable String id)
            throws NumberFormatException, EntityNotFound, NotAllowedException {
        return heistService.getConfirmedHeistMembers(id);
    }

    @GetMapping("/heist/{id}/skills")
    public List<SkillsHeist> getHeistSkills(@PathVariable String id) throws NumberFormatException, EntityNotFound {
        return heistService.getHeistSkills(id);
    }

    @GetMapping("/heist/{id}/status")
    public Map<String, HeistStatus> getHeistStatus(@PathVariable String id)
            throws NumberFormatException, EntityNotFound {
        // Get heist status
        return heistService.getHeistStatus(id);
    }

    @PostMapping("/heist")
    public ResponseEntity<Void> createHeist(@RequestBody @Valid Heist heist)
            throws DataIntegrityException, ValidationError {
        Long id = heistService.createHeist(heist).getId();
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Location", String.format("/heist/%s", id));
        return new ResponseEntity<>(responseHeaders, HttpStatus.CREATED);
    }

    @PatchMapping("/heist/{id}/skills")
    public ResponseEntity<Void> updateSkills(@RequestBody HeistSkillsDTO heistSkills, @PathVariable String id)
            throws NumberFormatException, EntityNotFound, UpdateNotAllowed, DataIntegrityException, ValidationError {
        heistService.updateSkills(heistSkills, id);
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-Location", String.format("/heist/%s/skills", id));
        return new ResponseEntity<>(responseHeaders, HttpStatus.NO_CONTENT);
    }

    @GetMapping("/heist/{id}/eligible_members")
    public EligibleMembersDTO getEligibleMembers(@PathVariable String id) throws NumberFormatException, EntityNotFound,
            NotAllowedException {
        return heistService.getEligibleMembers(id);
    }

    @PutMapping("/heist/{id}/members")
    public ResponseEntity<Void> confirmMembers(@RequestBody ConfirmMembersDTO members, @PathVariable String id)
            throws NumberFormatException, EntityNotFound, UpdateNotAllowed, NotAllowedException,
            BadRequestException {
        heistService.confirmMembers(members.getMembers(), id);
        HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.set("Content-Location", String.format("/heist/%s/skills", id));
        return new ResponseEntity<>(responseHeaders,HttpStatus.NO_CONTENT);
    }
    @PutMapping("/heist/{id}/start")
    public ResponseEntity<Void> startHeist(@PathVariable String id)
            throws NumberFormatException, EntityNotFound, NotAllowedException {
        heistService.startHeist(id);
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Location", String.format("/heist/%s/status", id));
        return new ResponseEntity<>(responseHeaders,HttpStatus.NO_CONTENT);
        }

    
}
