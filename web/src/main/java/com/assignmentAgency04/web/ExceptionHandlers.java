package com.assignmentAgency04.web;

import com.assignmentAgency04.core.exceptions.BadRequestException;
import com.assignmentAgency04.core.exceptions.DataIntegrityException;
import com.assignmentAgency04.core.exceptions.EntityNotFound;
import com.assignmentAgency04.core.exceptions.NotAllowedException;
import com.assignmentAgency04.core.exceptions.UpdateNotAllowed;
import com.assignmentAgency04.core.exceptions.ValidationError;

import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ExceptionHandlers {
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Void> handleValidationExceptions(
    MethodArgumentNotValidException ex) {
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<String> handleMessageNotReadable(HttpMessageNotReadableException e) {
        return new ResponseEntity<String>("Request message not readable", HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(DataIntegrityException.class)
    public ResponseEntity<Void> handleDataIntegrityException(DataIntegrityException ex) {
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ValidationError.class)
    public ResponseEntity<Void> handleValidationError(ValidationError ex) {
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<Void> handleBadRequestException(BadRequestException ex) {
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UpdateNotAllowed.class)
    public ResponseEntity<Void> handleUpdateNotAllowed(UpdateNotAllowed ex) {
        return new ResponseEntity<>(HttpStatus.METHOD_NOT_ALLOWED);
    }
    @ExceptionHandler(NotAllowedException.class)
    public ResponseEntity<Void> handleNotAllowedException(NotAllowedException ex) {
        return new ResponseEntity<>(HttpStatus.METHOD_NOT_ALLOWED);
    }

    @ExceptionHandler(EntityNotFound.class)
    public ResponseEntity<Void> handleEntityNotFound(EntityNotFound ex) {
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}
